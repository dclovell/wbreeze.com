#! /bin/sh
datestamp=`date "+%Y%m%d%H%M%S"`
bkpfilename="bkp/db_$1_${datestamp}.sql"
[ -e bkp ] || mkdir bkp
mysqldump --user wbreezec_admin --result_file "${bkpfilename}" -p $1
gzip "${bkpfilename}"
echo "mysqldump of $1 to ${bkpfilename}"
ls -l bkp
