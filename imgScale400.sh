#!/bin/bash

if [ "$1" == "" -o ! -d "$1" ]; then
  echo "Specify a directory"
  exit 1
fi

SOURCE_DIR="$1"

for PREVIEW_SIZE in 400
do
DEST_DIR="$SOURCE_DIR/$PREVIEW_SIZE"
[ -e "$DEST_DIR" ] || mkdir -p "$DEST_DIR"
if [ -d "$DEST_DIR" ]; then
  # make preview and thumbnail images
  while IFS= read -d $'\0' -r SRC_IMAGE ; do
    CURF=${SRC_IMAGE##*/}
    DEST_PREVIEW="$DEST_DIR/$CURF"
    [ "$SRC_IMAGE" -nt "$DEST_PREVIEW" ] && echo "$DEST_PREVIEW" && magick "$SRC_IMAGE" \
      -auto-orient \
      -resize ${PREVIEW_SIZE}x${PREVIEW_SIZE} \
      "$DEST_PREVIEW"
  done < <(find -L "$SOURCE_DIR" -maxdepth 1 \( -iname '*.jpg' \
     -o -iname '*.jpeg' \
     -o -iname '*.gif' \
     -o -iname '*.png' \) \
     -a -type f -print0 | sort -z )
else
  echo 'failed to create scaled image directory. file in the way?'
fi
done
