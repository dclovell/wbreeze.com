# WBreeze.com

See [WBreeze.com on AWS](
  https://wnw.wbreeze.com/2022/07/WBreeze-com-on-AWS.html
)

## How the site works

The site uses XSL transforms to produce HTML files from XML markup.  The
majority of the pages are files with extension "xml". These are the files that
you edit. The transformed files served for viewing are files with extension,
"html".

The site uses a build system, [Ant][ant] from the [Apache Software
Foundation][apache]. (It is an old Java thing.) The file "build.xml" in the
"www" directory configures the build. To "build" the site-- namely, to produce
the HTML files from the edited XML markup --open a terminal, switch to the
`www` directory and run `ant`. Just like that.

- `cd www`
- `ant`

[ant]: https://ant.apache.org/
[apache]: https://www.apache.org/

All of the derived "html" files are kept under version control.

On the Mac, I install the `ant` system using Homebrew: `brew install ant`.
It has a dependency on a Java runtime because it was developed with the Java
programming language in the Java ecosystem. The Homebrew install takes care
of installing that dependency.

## To update the AWS "server"

We maintain an AWS S3 bucket with the content for the site. All that is
required is to sync copy the files to that S3 bucket. The base directory has a
script for that, `cp2s3.sh`.


## How the galleries work

Load up original photos in the `www/gallery` directory.
Only original photos go here. Organize them in directories as wanted.

  - The `family` directory is protected by HTTP Basic Authentication
  - The `shared` directory is not linked from anything in `wbreeze.com`
  - The `public` directory is linked as a gallery in `wbreeze.com`

## To generate the galleries at `wbreeze.com`

Run the corresponding `generate_*_album.sh` script, e.g.
`./generate_public_album.sh` OR
generate all of the galleries with `./generate_photo_albums.sh`

The output of the `generate...` scripts will go to `www/photos`.
Thus:

- `www/photos` contains generated content xml, html, and scaled images
- `www/gallery` contains original photos only

Neither `www/gallery` nor `www/photos` is kept under version control.

