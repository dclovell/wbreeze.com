var image_index = 0;
var image_size = 0;
function Show_Image (index)
{
    image_index = index;
    var size_dir = ".";
    if (0 <= image_size && image_size < (image_sizes.length-1)) {
       size_dir = image_sizes[image_size];
    }
    var img = document.getElementById("img");
    var showing = document.getElementById("showing");
    var caption = document.getElementById("caption");
    var credit = document.getElementById("credit");
    img.src = image_dir + "/" + size_dir + "/" + image_list [index];
    showing_str = (index + 1) + " of " + (image_list.length) + " : " + image_list[index];
    showing.innerHTML = showing_str;
    caption.innerHTML = image_captions [index];
    credit.innerHTML = image_credits [index];
}

function Show_Size(index)
{
    image_size = index;
    Show_Image(image_index);
}

function Slideshow(index)
{
    Show_Image(index);
    var album_index = document.getElementById("album_index");
    var slideshow = document.getElementById("slideshow");
    album_index.style.display="none";
    slideshow.style.display="block";
}
     
function Goto_Index()
{
    var album_index = document.getElementById("album_index");
    var slideshow = document.getElementById("slideshow");
    slideshow.style.display="none";
    album_index.style.display="block";
}

function Show_Prev ()
{
    if (image_index > 0) 
    {
        image_index--;
    }
    else
    {
        image_index = image_list.length-1;
    }
    Show_Image (image_index);
}

function Show_Next ()
{
    if (image_index < image_list.length - 1)
    {
       image_index++;
    }
    else
    {
       image_index = 0;
    }
    Show_Image (image_index);
}
