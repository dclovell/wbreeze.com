<?xml version="1.0" encoding="UTF-8"?>
<!--
Stylesheet takes care of page layout settings.
It ought to provide a default box size.
It must provide three named templates, all called from the root node.
- head-content provides content inside the head tag
- favicon content also reproduced by site.xsl inside of the head tag
- body-attributes provides attribute content for the body element
- body-content provides markup content for the body element
The third template, body-content may contaian an <apply-templates/> to
process source document elements below the root; otherwise, the stylesheet
will process no such elements.
-->
<xsl:stylesheet version='1.0' xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:param name="box-size">400</xsl:param>

<xsl:template name="head-content">
    <link rel="stylesheet" type="text/css" href="/styles.css"/>
</xsl:template>

<xsl:template name="favicon-content">
  <link rel="apple-touch-icon" sizes="180x180" href="/favicon/apple-touch-icon.png"/>
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon/favicon-16x16.png"/>
  <link rel="manifest" href="/favicon/site.webmanifest"/>
  <link rel="mask-icon" href="/favicon/safari-pinned-tab.svg" color="#5bbad5"/>
  <link rel="shortcut icon" href="/favicon/favicon.ico"/>
  <meta name="msapplication-TileColor" content="#2d89ef"/>
  <meta name="msapplication-config" content="/favicon/browserconfig.xml"/>
  <meta name="theme-color" content="#a9d4f2"/>
</xsl:template>

<xsl:template name="body-attributes"/>

<xsl:template name="body-content">
   <xsl:apply-templates/>
   <p class="copyright">Copyright (c) 2001 - 2025 Douglas Lovell</p>
</xsl:template>

</xsl:stylesheet>
