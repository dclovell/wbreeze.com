<?xml version="1.0" encoding="UTF-8"?>
<!--
  Stylesheet creates a story from a photostory element.  A photostory is a series
  of narratives alongside images. Supports markup of the form:
  
  <photostory text-side="left" box-size="400" image-loc="/images">
  <storyItem image-loc="."  title="optional">
  <image image-loc="." name="name" caption="caption"/> (zero or more)
  ... content ...
  </storyItem>
  </photostory>
  
  text-side indicates whether text goes on the left or right in the story.
  box-size indicates the size of the enclosing box for images.
  image-loc indicates the default location for images.
  title is an optional section title for the story item.
  The example shows defaults for these attributes.
  The storyItem and image elements may override the image-loc.
  The image caption is optional.  The name is not.
  
  The result markup has form:
  
  <table class="photostory">
  <tbody class="photostory">
  <tr class="photostory">
  <td class="photostory-tcell">
  ... content ...
  </td>
  <td class="photostory-icell">
  <div class="photostory-image-block">
  <a href="@image-loc/@name">
  <img class="photostory-image" 
  src="@image-loc/@box-size/@name" 
  alt="@caption"/>
  </a>
  <span class="photostory-image-caption">
  @caption
  </span>
  </div>
  </td>
  </tr>
  </tbody>
  </table>
-->
<xsl:stylesheet version='1.0'
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="photostory">
    <div class="photostory">
      <xsl:apply-templates>
        <xsl:with-param name="text-side">
          <xsl:choose>
            <xsl:when test="@text-side">
              <xsl:value-of select="@text-side" />
            </xsl:when>
            <xsl:otherwise>left</xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="box-size">
          <xsl:choose>
            <xsl:when test="@box-size">
              <xsl:value-of select="@box-size" />
            </xsl:when>
            <xsl:otherwise>400</xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
        <xsl:with-param name="image-loc">
          <xsl:choose>
            <xsl:when test="@image-loc">
              <xsl:value-of select="@image-loc" />
            </xsl:when>
            <xsl:otherwise>.</xsl:otherwise>
          </xsl:choose>
        </xsl:with-param>
      </xsl:apply-templates>
    </div>
  </xsl:template>

  <xsl:template match="storyItem|storyitem">
    <xsl:param name="image-loc">.</xsl:param>
    <xsl:param name="box-size">400</xsl:param>
    <xsl:param name="text-side">left</xsl:param>
    <xsl:variable name="side">
      <xsl:choose>
        <xsl:when test="@text-side">
          <xsl:value-of select="@text-side" />
        </xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="$text-side" />
        </xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <div class="photostory-entry">
      <xsl:choose>
        <xsl:when test="$side='left'">
          <div class="photostory-tcell">
              <xsl:call-template name="storyItemTitle"/>
            <xsl:apply-templates
              select="text()|*[not(self::image)]" />
          </div>
          <div class="photostory-icell">
            <xsl:apply-templates select="image">
              <xsl:with-param name="box-size">
                <xsl:value-of select="$box-size" />
              </xsl:with-param>
              <xsl:with-param name="image-loc">
                <xsl:value-of select="$image-loc" />
              </xsl:with-param>
            </xsl:apply-templates>
          </div>
        </xsl:when>
        <xsl:otherwise>
          <div class="photostory-icell">
            <xsl:apply-templates select="image">
              <xsl:with-param name="box-size">
                <xsl:value-of select="$box-size" />
              </xsl:with-param>
              <xsl:with-param name="image-loc">
                <xsl:value-of select="$image-loc" />
              </xsl:with-param>
            </xsl:apply-templates>
          </div>
          <div class="photostory-tcell">
              <xsl:call-template name="storyItemTitle"/>
            <xsl:apply-templates
              select="text()|*[not(self::image)]" />
          </div>
        </xsl:otherwise>
      </xsl:choose>
    </div>
  </xsl:template>

  <xsl:template name="storyItemTitle">
    <xsl:if test="@title">
           <h2 class="storyItem-title"><xsl:value-of select="@title"/></h2>
    </xsl:if>
  </xsl:template>
  
</xsl:stylesheet>
