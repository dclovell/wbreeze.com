<?xml version="1.0" encoding="UTF-8"?>
<!--
	Stylesheet creates a test from a quiz element. Supports markup of the form:
	
	
	The result markup has form:
	
-->
<xsl:stylesheet version='1.0'
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

  <xsl:template match="quiz[position()=1]" mode="head">
      <link rel="stylesheet" type="text/css" href="/style/quiz.css"/>
  </xsl:template>
     
  <xsl:template match="quiz">
    <form class="quiz">
      <xsl:if test="@action">
         <xsl:attribute name="action">
            <xsl:value-of select="@action"/>
         </xsl:attribute>
      </xsl:if>
      <xsl:if test="@method">
         <xsl:attribute name="method">
            <xsl:value-of select="@method"/>
         </xsl:attribute>
      </xsl:if>
      <ol>
        <xsl:apply-templates>
          <xsl:with-param name="quiz">
            <xsl:value-of select="@name"/>
          </xsl:with-param>
        </xsl:apply-templates>
      </ol>
    </form>
  </xsl:template>

  <xsl:template match="question">
    <xsl:param name="quiz"/>
    <xsl:variable name="question">
      <xsl:value-of select="$quiz"/>
      <xsl:text>-q</xsl:text>
      <xsl:value-of select="position()"/>
    </xsl:variable>
    <li class="question">
      <xsl:apply-templates select="text() | *[name() != 'alt']"/>
      <div class="alternatives">
        <xsl:choose>
          <xsl:when test="@type='mult'">
            <xsl:apply-templates select="alt" mode="mult">
              <xsl:with-param name="number">
                <xsl:value-of select="$question"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </xsl:when>
          <xsl:when test="@type='one'">
            <xsl:apply-templates select="alt" mode="one">
              <xsl:with-param name="number">
                <xsl:value-of select="$question"/>
              </xsl:with-param>
            </xsl:apply-templates>
          </xsl:when>
          <xsl:when test="@type='tf'">
            <xsl:call-template name="tf">
              <xsl:with-param name="number">
                <xsl:value-of select="$question"/>
              </xsl:with-param>
            </xsl:call-template>
          </xsl:when>
        </xsl:choose>
      </div>
    </li>
  </xsl:template>
  
  <xsl:template match="blank[@length &lt;= 60]">
    <xsl:param name="number"/>
    <input class="blank" type="text">
      <xsl:attribute name="name">
        <xsl:value-of select="$number"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:if test="@length">
        <xsl:attribute name="size">
          <xsl:value-of select="@length"/>
        </xsl:attribute>
      </xsl:if>
    </input>
  </xsl:template>

  <xsl:template match="blank[@length > 60]">
    <xsl:param name="number"/>
    <p class="long-answer">
    <textarea class="blank" type="text">
      <xsl:attribute name="name">
        <xsl:value-of select="$number"/>
        <xsl:text>-</xsl:text>
        <xsl:value-of select="@name"/>
      </xsl:attribute>
      <xsl:attribute name="size">
        <xsl:value-of select="@length"/>
      </xsl:attribute>
      <xsl:attribute name="rows">
        <xsl:value-of select="floor(@length div 60) + 1"/>
      </xsl:attribute>
      <xsl:attribute name="cols">
        <xsl:text>60</xsl:text>
      </xsl:attribute>
      <xsl:text> </xsl:text>
    </textarea>
    </p>
  </xsl:template>

  <xsl:template match="alt" mode="mult">
    <xsl:param name="number"/>
    <div>
      <xsl:choose>
        <xsl:when test="string-length(./text()) &lt; 15">
          <xsl:attribute name="class">
            <xsl:text>mult</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class">
            <xsl:text>long-mult</xsl:text>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <input type="checkbox">
        <xsl:attribute name="name">
          <xsl:value-of select="$number"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="@value"/>
        </xsl:attribute>
        <xsl:attribute name="name">
          <xsl:value-of select="$number"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="@value"/>
        </xsl:attribute>
      </input>
      <label>
        <xsl:attribute name="for">
          <xsl:value-of select="$number"/>
          <xsl:text>-</xsl:text>
          <xsl:value-of select="@value"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </label>
    </div>
  </xsl:template>
  
  <xsl:template match="alt" mode="one">
    <xsl:param name="number"/>
    <xsl:variable name="id">
      <xsl:value-of select="$number"/>
      <xsl:text>-</xsl:text>
      <xsl:value-of select="@value"/>
    </xsl:variable>
    <div>
      <xsl:choose>
        <xsl:when test="string-length(./text()) &lt; 15">
          <xsl:attribute name="class">
            <xsl:text>one</xsl:text>
          </xsl:attribute>
        </xsl:when>
        <xsl:otherwise>
          <xsl:attribute name="class">
            <xsl:text>long-one</xsl:text>
          </xsl:attribute>
        </xsl:otherwise>
      </xsl:choose>
      <input type="radio">
        <xsl:attribute name="name">
          <xsl:value-of select="$number"/>
        </xsl:attribute>
        <xsl:attribute name="value">
          <xsl:value-of select="@value"/>
        </xsl:attribute>
        <xsl:attribute name="id">
          <xsl:value-of select="$id"/>
        </xsl:attribute>
      </input>
      <label>
        <xsl:attribute name="for">
          <xsl:value-of select="$id"/>
        </xsl:attribute>
        <xsl:apply-templates/>
      </label>
    </div>
  </xsl:template>
  
  <xsl:template name="tf">
    <xsl:param name="number"/>
    <xsl:variable name="bname">
      <xsl:value-of select="$number"/>
    </xsl:variable>
    <xsl:variable name="id">
      <xsl:value-of select="$number"/>
    </xsl:variable>
    <input class="tf" type="radio">
      <xsl:attribute name="name">
        <xsl:value-of select="$bname"/>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="$id"/>
        <xsl:text>-t</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="value">y</xsl:attribute>
    </input>
    <label>
      <xsl:attribute name="for">
        <xsl:value-of select="$id"/>
        <xsl:text>-t</xsl:text>
      </xsl:attribute>
      <xsl:text>true</xsl:text>
    </label>
    <input class="tf" type="radio">
      <xsl:attribute name="name">
        <xsl:value-of select="$bname"/>
      </xsl:attribute>
      <xsl:attribute name="id">
        <xsl:value-of select="$id"/>
        <xsl:text>-f</xsl:text>
      </xsl:attribute>
      <xsl:attribute name="value">n</xsl:attribute>
    </input>
    <label>
      <xsl:attribute name="for">
        <xsl:value-of select="$id"/>
        <xsl:text>-f</xsl:text>
      </xsl:attribute>
      <xsl:text>false</xsl:text>
    </label>
  </xsl:template>
  
</xsl:stylesheet>
