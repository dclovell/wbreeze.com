BEGIN	{ print "<album title=\"\" image-loc=\"images\" thumbnail-loc=\"200\">";
	  print "<sizes>";
	  print "<size box-size='400'/>";
	  print "<size box-size='600'/>";
	  print "<size box-size='800'/>";
	  print "<size box-size='1000'/>";
	  print "<size box-size='original'/>";
	  print "</sizes>";
	}
	{ print "<image name=\"" $1 "\"/>"; }
END	{ print "</album>"; }
