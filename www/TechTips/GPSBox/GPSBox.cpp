#include <stdio.h>
#include <math.h>

int atoi(char *);

// Radians to degrees conversion

long double RadToDeg(long double radians)
{
  return (radians*180.0)/M_PI;
}

// Degrees to radians conversion

long double DegToRad(long double degrees)
{
  return (degrees*M_PI)/180.0;
}

//
// This class represents a lon and lat pair.
//

typedef struct {
		  long double lon;
		  long double lat;
  } POSITION;

//
// Compute the distance between two positions in lat/long form. Must convert to
// radian angles,compute a radial distance in radians, then map back to kilometers
// based on the approximate earths radius in kilometers.
//
long double LatLonDistance(POSITION p1, POSITION p2)
{
  long double L1r = DegToRad(p1.lon);
  long double l1r = DegToRad(p1.lat);
  long double L2r = DegToRad(p2.lon);
  long double l2r = DegToRad(p2.lat);
  long double dlr, term1, term2, d, r, val;
  long double dLr;

  dlr = l2r - l1r;
  dLr = L2r - L1r;

  if (dlr < 0.0) dlr = - dlr;
  if (dLr < 0.0) dLr = - dLr;

  term1 = sinl(dlr/(long double)2.0);
  term2 = sinl(dLr/(long double)2.0);

  val = sqrtl(term1*term1 + cosl(l1r)*cosl(l2r)*term2*term2);

  if (val > 1.0) val = 1.0;

  d = (long double)2.0 * asinl(val);
  r = 6367.0; // earth radius in kilometers

  return (r * d);
}

//
// Given a position p1 and an angle and distance from it, compute the new
// position. Must convert everything to unit sphere distances in radians
// and angles in radians.
//
void ComputeCorner(long double angle, long double distance,
						 POSITION p1, POSITION *p2)
{
  long double L1r = DegToRad(p1.lon);
  long double l1r = DegToRad(p1.lat);
  long double anr = DegToRad(angle);
  long double disr = distance / 6367;
  long double l2r = asinl(sinl(l1r)*cosl(disr)+cosl(l1r)*sinl(disr)*cosl(anr));
  long double L2r = fmodl(L1r-asinl(sinl(anr)*sinl(disr)/cosl(l2r))+M_PI,2*M_PI)-M_PI;

  p2->lon = RadToDeg(L2r);
  p2->lat = RadToDeg(l2r);
}

//
// Print the markers we can format these things for different GPS receivers
// as waypoints.
//
void PrintMarker (char *s, POSITION p)
{  char *t;
	char  obuf[256];
   char ns, ew;

	int latdeg, latmin, latsec, lrc = 0;
	int londeg, lonmin, lonsec;

	long double minutes, seconds;

	if (p.lat < 0.0) { ns = 'S'; p.lat = -p.lat; } else ns = 'N';

	latdeg = p.lat;
	minutes = (p.lat - latdeg) * (long double) 60.0;
	latmin = minutes;
	seconds = (minutes - latmin) * (long double) 1000.0;
	latsec = seconds;

	if (p.lon < 0.0) { ew = 'E'; p.lon = -p.lon; } else ew = 'W';
	londeg = p.lon;
	minutes = (p.lon - londeg) * (long double) 60.0;
	lonmin = minutes;
	seconds = (minutes - lonmin) * (long double) 1000.0;
	lonsec = seconds;

	sprintf(obuf,"PMGNWPL,%02d%02d.%03d,%c,%03d%02d.%03d,%c,0,M,%s,%s,a",
			  latdeg,latmin,latsec,ns,londeg,lonmin,lonsec,ew,s,"box");

	for(t = &obuf[0]; *t; t++) lrc = lrc ^ *t;

	printf("$%s*%02X\n", obuf, lrc);
}

//
// If d1 and d2 are not very close to each other print an error message.
// This is used to sanitize the coordinates of the box after the have been
// computed.
//

void DistanceCheck(char *s, double long d1, double long d2)
{
	  double long dif = d1 - d2;
	  if ((dif > 0.00001)||(dif < -0.00001))
			printf( s , (double) d1, (double) d2 );
}

//
// Ask for the coordinates of the front center of the box, then generate
// the coordinates of each of the markers, validate that they all make
// sense by taking distances between them using independent math and make
// sure the distances make sense.
//

void DoNewBox(POSITION p, double orient)
{
	double distance;

	POSITION box_FrontCenter = p;
	POSITION box_FrontRight;
	POSITION box_CenterRight;
	POSITION box_CenterCenter;
	POSITION box_RearRight;
	POSITION box_RearCenter;
	POSITION box_RearLeft;
	POSITION box_CenterLeft;
	POSITION box_FrontLeft;
	POSITION box_Judges;

	PrintMarker("XXFC",  box_FrontCenter);

   // Front right corner bears 'orient' degrees 1/2 km from us.
	ComputeCorner(orient, 0.5, box_FrontCenter, &box_FrontRight);
	PrintMarker("XXFR",  box_FrontRight);

	// Center right corner is -45 degrees and sqrt(0.5*0.5+0.5*0.5) away
	ComputeCorner(orient-45.0, sqrtl(0.5), box_FrontCenter, &box_CenterRight);
	PrintMarker("XXCR",  box_CenterRight);

	// Rear right corner is -60 degrees and sqrt(0.5*0.5+1.0*1.0) away
	ComputeCorner(orient-RadToDeg(atanl(2.0)), sqrtl(1.25), box_FrontCenter, &box_RearRight);
	PrintMarker("XXRR",  box_RearRight);

	// Rear center is -90 from and 1.0 km away.
	ComputeCorner(orient-90.0, 1.0, box_FrontCenter, &box_RearCenter);
	PrintMarker("XXRC",  box_RearCenter);

   // Center box is -90 degrees away and .5 km away
	ComputeCorner(orient-90.0, 0.5, box_FrontCenter, &box_CenterCenter);
	PrintMarker("XXCC",  box_CenterCenter);

	// Rear right corner is 180+60 degrees and sqrt(0.5*0.5+1.0*1.0) away
	ComputeCorner(orient+180+RadToDeg(atanl(2.0)), sqrtl(1.25), box_FrontCenter, &box_RearLeft);
	PrintMarker("XXRL",  box_RearLeft);

   ComputeCorner(orient+180+45, sqrtl(0.5), box_FrontCenter, &box_CenterLeft);
	PrintMarker("XXCL",  box_CenterLeft);

	ComputeCorner(orient+180, 0.5, box_FrontCenter, &box_FrontLeft);
	PrintMarker("XXFL",  box_FrontLeft);

	// Judges are located  90 degrees around behind us and 1/3 km back (900')
	ComputeCorner(orient+90.0, 0.333, box_FrontCenter, &box_Judges);
   PrintMarker("XXJU",  box_Judges);

	// Sanity check. Make sure some key distances all make sense.
	distance = LatLonDistance( box_FrontLeft, box_FrontRight);
	DistanceCheck("FL - FR = %lf(%lf)\n", distance, 1.0);

	distance = LatLonDistance(box_FrontRight, box_RearRight);
	DistanceCheck("FR - RR = %lf(%lf)\n", distance, 1.0);

	distance = LatLonDistance(box_RearLeft, box_RearRight);
	DistanceCheck("RL - RR = %lf(%lf)\n", distance, 1.0);

	distance = LatLonDistance(box_RearLeft, box_FrontLeft);
	DistanceCheck("RL - FL = %lf(%lf)\n", distance, 1.0);

	distance = LatLonDistance(box_CenterLeft, box_CenterRight);
	DistanceCheck("CL - CR = %lf(%lf)\n", distance, 1.0);

	distance = LatLonDistance(box_RearLeft, box_FrontRight);
	DistanceCheck("RL - FR = %lf(%lf)\n", distance, sqrtl(2.0));

	distance = LatLonDistance(box_RearRight, box_FrontLeft);
	DistanceCheck("RR - FL = %lf(%lf)\n", distance, sqrtl(2.0));

}

//
// Given an input string of the form:
//  "N 44 33.7 W 74 22.4 092"
//
// Build the position lat/lon and orientation and return true.
// Otherwise print a meaningful error and return.
//
int ExtractBox(char b[], POSITION *p, long double *orient)
{
	 char   ns[2], ew[2];
	 double latmin, lonmin;
	 int    latdeg, londeg, heading;
	 int    invlat, invlon;

	 ns[1] = ew[1] = '\0';

	 if (sscanf(b,"%1s %d %lf %1s %d %lf %d",
					ns, &latdeg, &latmin,
					ew, &londeg, &lonmin,
				  &heading) != 7) {

		  printf("*** expecting format like: N 44 33.7 W 74 22.4 092\n");
		  return 0;
	 } else {
		  if (heading >= 360 || heading < 0) {
				printf("***invalid orientation %d must be 0-360\n", heading);
				return 0;
		  }
		  *orient = heading;

		  if (latmin >= 60.0 || latmin < 0.0) {
				printf("***latitude minutes %lf must be 0.0-60.0\n", latmin);
            return 0;
		  }

		  if (lonmin >= 60.0 || lonmin < 0.0) {
				printf("***longitude minutes %lf must be 0.0-60.0\n", lonmin);
            return 0;
		  }

		  if (latdeg >= 90 || latdeg < 0) {
				printf("***latitude degrees %d must be 0-90\n", latdeg);
            return 0;
		  }

		  if (londeg >= 180 || londeg < 0) {
				printf("***longitude degrees %d must be 0-180\n", londeg);
            return 0;
		  }

		  invlat = 0;
		  if (ns[0] == 'S' || ns[0] == 's') {
				invlat = 1;
		  } else {
				if (ns[0] != 'N' && ns[0] != 'n') {
					 printf("***latitude must be N or S, was '%c'\n", ns[0]);
					 return 0;
				}
		  }

		  invlon = 0;
		  if (ew[0] == 'E' || ew[0] == 'e') {
				invlon = 1;
		  } else {
				if (ew[0] != 'W' && ew[0] != 'w') {
					 printf("***longitude must be W or E, was '%c'\n", ew[0]);
					 return 0;
				}
		  }

		  p->lat = (long double) latdeg;
		  if (latmin > 0.0) p->lat += latmin/(long double)60.0;

		  p->lon = (long double) londeg;
		  if (lonmin > 0.0) p->lon += lonmin/(long double)60.0;

		  if (invlat) p->lat = -p->lat;
		  if (invlon) p->lon = -p->lon;

		  return(1);
	 }
}

void DrawBoxDiagram(int or)
{
	  printf("           __        ___         __ \n");
	  printf("          |           |            |\n");
	  printf("         XXRL        XXRC        XXRR\n\n");

	  printf("          |-         -|-          -|\n");
	  printf("         XXCL        XXCC        XXCR\n\n");

	  printf("          |_         _|_          _|  ________\\ %03d TRUE\n",or);
	  printf("         XXFL        XXFC        XXFR         /\n\n");
	  printf("                     XXJU\n");
}

main()
{
   char buffer[256];

	POSITION box_FrontCenter;
	long double orientation;



	printf(" AEROBATIC BOX COORDINATE CALCULATOR\n");
	printf(" V1.0 (C) Feb 2002 Peter Ashwood-Smith\n");
	printf(" -------------------------------------\n\n");
	printf(" Enter the lat/long of the box front center followed by the TRUE heading\n");
	printf(" to the front right marker from the box front center marker, then hit\n");
	printf(" return. The tool will compute the coordinates of 9 waypoints including\n");
	printf(" the judges position. For example:\n");

	printf(" ==> N 44 33.7 W 74 22.4 092\n\n");


	buffer[0] = '\0';
	printf(" ==> ");
	fflush(stdout);

	while(gets(buffer) == &buffer[0]) {

			if (ExtractBox(buffer, &box_FrontCenter, &orientation)) {
				 DoNewBox(box_FrontCenter, orientation);
				 DrawBoxDiagram((int) orientation);
			}

			buffer[0] = '\0';
			printf("\n ==> ");
			fflush(stdout);
   }


}

