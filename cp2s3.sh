#!/bin/bash

# WB_DISTR_SRC_DIR="www"
# WB_DISTR_DEST_DIR="s3://wbreeze.com-bucket"
if [[ -n ${WB_DISTR_SRC_DIR} && -n ${WB_DISTR_DEST_DIR} ]]; then
  aws s3 sync "${WB_DISTR_SRC_DIR}" "${WB_DISTR_DEST_DIR}" \
  --exclude "**/.DS_Store" --exclude ".DS_Store" \
  --exclude ".htaccess*" --exclude ".git*"
else
  echo "Missing environment variable"
  exit 2
fi
